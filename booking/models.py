# -*- coding: utf-8 -*-

from openerp import models, fields, api

class Tiket(models.Model):
    _name = 'booking.tiket'
    # _rec_name = 'asal'

    @api.multi
    def name_get(self):
        names = []
        for rec in self:
            names.append((rec.id, "%s - %s" % (rec.asal, rec.tujuan)))
            print "=========> names:", names
        return names

    @api.depends('pajak','harga')
    def _compute_pajak_harga(self):
        for record in self:
            self.pajak_harga = self.harga + self.pajak
    
    pajak_harga = fields.Char(compute='_compute_pajak_harga')

    asal = fields.Char(string="Asal")
    tujuan = fields.Char(string="Tujuan")
    harga = fields.Float()
    pajak = fields.Float()

    fasilitas_ids = fields.One2many(
        'booking.fasilitas', 'fasilitas_id', string="Fasilitas")
    additional_ids = fields.One2many(
        'booking.additional', 'additional_id', string="Additional")

class Transaction(models.Model):
    _name = 'booking.transaction'
    
    @api.multi
    def action_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        self.state = 'confirmed'

    @api.multi
    def action_done(self):
        self.state = 'done'

    tanggal = fields.Date()

    tiket_id = fields.Many2one('booking.tiket',
        ondelete='set null', string="Tiket")
    customer_id = fields.Many2one('res.partner',
        ondelete='set null', string="Customer")
    
    harga = fields.Float(string='Harga', related='tiket_id.harga')
    alamat = fields.Char(string='Alamat', related='customer_id.street')
    telepon = fields.Char(string='Telepon', related='customer_id.phone')

    state = fields.Selection([
        ('draft', "Draft"),
        ('confirmed', "Confirmed"),
        ('done', "Done"),
    ], default='draft')


class Fasilitas(models.Model):
    _name = 'booking.fasilitas'

    fasilitas_id = fields.Many2one('booking.tiket',
        ondelete='set null', string="Fasilitas")
    name = fields.Char(string="Nama")

class Additional(models.Model):
    _name = 'booking.additional'

    additional_id = fields.Many2one('booking.tiket',
        ondelete='set null', string="Additional")
    name = fields.Char(string="Nama")

class ResPartner(models.Model):
    _inherit = 'res.partner'

    # onchange handler
    @api.onchange('umur', 'jk')
    def _onchange_price(self):
        # set auto-changing field
        # self.price = self.umur * self.jk
        if self.jk :
            if self.jk == 'pria' :
                if self.umur >= 0 and self.umur <= 18 :
                    self.result = 'saudara'
                else:
                    self.result = 'bapak'
            else :
                if self.umur >= 0 and self.umur <= 18 :
                    self.result = 'saudari'
                else:
                    self.result = 'ibu'
        else:
        # Can optionally return a warning and domains
            return {
                'warning': {
                    'title': "Harap memilih jenis kelamin",
                    'message': "Field kosong",
                }
            }

    result = fields.Selection([
        ('saudara', "Saudara"),
        ('saudari', "Saudari"),
        ('bapak', "Bapak"),
        ('ibu', "Ibu")
    ])
    umur = fields.Integer()
    jk = fields.Selection([
        ('pria', "Pria"),
        ('wanita', "Wanita")
    ])
    customerRes = fields.One2many('booking.transaction', 'customer_id', string="Customer")
